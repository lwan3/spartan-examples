# ANSYS Job Submission Examples

## Introduction

All the examples here use the old build system and ANSYS_CFD v19.0 (2019).

The example OscillatingPlate is a classic from ANSYS involving a plate in a Fluid-Structure Interaction (FSI). The original example for High 
Performance Computing was from the Victorian Partnership for Advanced Computing (2015) using TORQUE PBS and then ported to the Univeristy of 
Melbourne using the Slurm Workload Manager.

All other examples come from the rather impressive ANSYS tutorial collection from the University of Alberta 
(https://sites.ualberta.ca/~wmoussa/AnsysTutorial/index.html). As should be evident from these, only part of ANSYS (the computation side for 
solving) is represented in batch processing. Interactive visualisation and post-processing interpretation requires a different approach.

## Directory Listing

The directories are as follows:

2dtruss: Basic functions will be shown to provide you with a general knowledge of command line codes.	

3dbike: Intermediate ANSYS functions will be shown in detail to provide you with a more general understanding of how to use ANSYS.

2dstress: Boolean operations, plane stress and uniform pressure loading will be introduced in the creation and analysis of this 2-Dimensional 
object.

Solid: This tutorial will introduce techniques such as filleting, extrusion, copying and working plane orienation to create 3-Dimensional 
objects.

SelfWeight: Incorporating the weight of an object into the finite element analysis is shown in this simple cantilever beam example.

DistLoad: The application of distributed loads and the use of element tables to extract datal.	

NonLinearA: A large moment is applied to the end of a cantilever beam to explore Geometric Nonlinear behaviour (large deformations).	

BucklingE: Use both Eigenvalue and Nonlinear methods are used to solve a simple buckling problem.	

BucklingN: Use both Eigenvalue and Nonlinear methods are used to solve a simple buckling problem.	

NonLinearB: How to include material nonlinearities in an ANSYS model.	

DynamicModal: Explore the modal analyis capabilities of ANSYS.	

DynamicHarmonic: Explore the harmonic analyis capabilities of ANSYS.	

DynamicTransient: Explore the transient analyis capabilities of ANSYS.	

ThermalPure: Analysis of a pure conduction boundary condition example.	

ThermalMix: Analysis of a Mixed Convection/Conduction/Insulated boundary condition example.	

ThermalTrans: Analysis of heat conduction over time.	
Axisymmetry: Utilizing axisymmetry to model a 3-D structure in 2-D to reduce computational time.	

Again, it is re-iterated, these are not complete tutorials for ANSYS, just batch examples for the solver computation portion of the various 
tasks. See the University of Alberta website for complete tutorials.

## Array

A sample directory-driven job array is provided that takes a list of ANSYS directories and runs the jobs simultaneously. This will save you a 
lot of time!

## Note for admins. 

There will be changes to dependent software due to change in toolchain. Attempts to use a more consistent toolchain for X11 with GCC/4.9.2 
result in libXp.so.6 shared object file not found. The modules we have in these examples, whilst truly ugly, does work.

Last update: LL20210216
