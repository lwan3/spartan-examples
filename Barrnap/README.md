Example derived from the Swedish University of Agricultural Sciences.

https://www.hadriengourle.com/tutorials/

Barrnap (BAsic Rapid Ribosomal RNA Predictor) predicts the location of ribosomal RNA genes in genomes.

A draft genome is provided. A search is conducted on the genome for rRNA genes. Since these genes are usually quite conserved across 
species/genera, it could give us a broad idea of the organism.

The bin, bin.2.fa, is also used for the diamond example.


