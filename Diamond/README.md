Diamond is a sequence aligner for protein and translated DNA searches and functions as a drop-in replacement for the NCBI BLAST 
software tools. It is suitable for protein-protein search as well as DNA-protein search on short reads and longer sequences 
including contigs and assemblies, providing a speedup of BLAST ranging up to x20,000.

Use diamond against the swissprot database for quickly assigning taxonomy to our contigs.

It is very possible the swissprot database is too small for finding meaningful hits for undersequenced / poorly known organisms.

