Derived from the University of Agricultural Science, Sweden

The first dataset you will be working with is from an Illumina MiSeq dataset. The sequenced organism is an enterohaemorrhagic E. 
coli (EHEC) of the serotype O157, a potentially fatal gastrointestinal pathogen. The sequenced bacterium was part of an outbreak 
investigation in the St. Louis area, USA in 2011. The sequencing was done as paired-end 2x150bp.

The raw data were deposited at the European Nucleotide Archive, under the accession number SRR957824. A subset of the original 
dataset for this tutorial.

FastQC is used to check the quality of the data. There are two example job submission scripts. The first is on the raw data, the 
second after running Scythe (qv) and Sickle (qv) to remove poor quality bases.



