IDENTIFICATION DIVISION.
PROGRAM-ID. All_About_Divsions.

DATA DIVISION.
   WORKING-STORAGE SECTION.
   01 WS-NAME PIC A(30).
   01 WS-TITLE PIC A(30).
   01 WS-ID PIC 9(8) VALUE 20160716.

PROCEDURE DIVISION.
   A000-FIRST-PARA.
   DISPLAY 'Linux Users of Victoria Beginners Workshop'.
   MOVE 'Lev Lafayette' TO WS-NAME.
   MOVE 'GnuCOBOL' TO WS-TITLE.
   DISPLAY "Today's presenter is : "WS-NAME.
   DISPLAY "Today's presentation is : "WS-TITLE.
   DISPLAY "Today's date is : " WS-ID.
STOP RUN.
