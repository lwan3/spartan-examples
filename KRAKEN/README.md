Derived from a tutorial from the Swedish Univeristy of Agricultural Sciences.

https://www.hadriengourle.com/tutorials/

Kraken is a system for assigning taxonomic labels to short DNA sequences (i.e. reads) Kraken aims to achieve high sensitivity and 
high speed by utilizing exact alignments of k-mers and a novel classification algorithm.

Kraken uses a new approach with exact k-mer matching to assign taxonomy to short reads. It is extremely fast compared to 
traditional approaches (i.e. BLAST).

In this example samples are compared from the Pig Gut Microbiome to samples from the Human Gut Microbiome.

Whole Metagenome Sequencing, "shotgun metagenome sequencing", is used, a relatively new and powerful sequencing approach that 
provides insight into community biodiversity and function. On the contrary of Metabarcoding, where only a specific region of the 
bacterial community (the 16s rRNA) is sequenced, WMS aims at sequencing all the genomic material present in the environment.

The dataset (subset_wms.tar.gz) uses a sample of 3 pigs and 3 humans.

FastQC is used to check the quality of our data.

Kraken includes a script called kraken-report to transform this file into a "tree" view with the percentage of reads assigned to 
each taxa. Once the job is run take a look at the _tax.txt files in the results directory.
