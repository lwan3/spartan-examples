# Math Basics

Often one needs to engage in some relatively simple calculations on the Linux command line interactively or in a script. Rather than 
powerful a fully-fledged environment, such as Octave, R, Maxima, Julia, etc, or even writing one's own program, one can make use of 
various Linux utilities and lightweight applications instead.

The main tools for this include shell arithmetic, the utilities `expr`, `bc`, `awk`, and `datamash`. This document includes 
consideration of `expr`, `bc`, and `datamash` with `awk` covered in the RegEx directory (`/usr/local/common/RegEx`).

## Shell Arithmetic

The following examples are from the "Supercomputing with Linux" book as part of this series and illustrates the the simple integer arithmetic used in the bash shell through a loop structure.

`x=1; while [ $x -le 5 ]; do echo "While-do count up $x"; x=$(( $x + 1 )); done`
`x=5; until [ $x -le 0 ]; do echo "Until-do count down $x"; x=$(( $x - 1 )); done`
`x=1; until [ $x = 6 ]; do echo "Until-do count up $x"; x=$(( $x + 1 )); done`

The format for bash shell arithmetic is `$(( expression ))`. The shell expansion returns the result the given expression. Some simple examples making use of a variable follows:

```
$ x=5 && echo $x			# Sets x to 5.
$ echo $((x+2))				# Returns 7, x is still 5.
$ x=$((x+3)) && echo $x			# Sets x to 8.
$ ((x+=3)) && echo $x			# Sets x to 11.
$ echo $((x++)) && echo $x		# Echo x, increment by 1, echo x again.
$ echo $((++x)) && echo $x		# Increment by 1, echo x.
$ echo $((--x)) && echo $x		# Decrement by 1, echo x.
$ x=$((x/4)) && echo $x			# Division
$ x=$((x*4)) && echo $x			# Multiplication
$ x=$((x%4)) && echo $x			# Modulus
$ x=3 && ((x=x**2)) && echo $x		# Set x to 3, exponent ^2, echo result.
$ echo $((x=4,y=5,z=x*y,w=z/2))		# Multiple statements must be comma-separated. Shell gives last expression.
$ echo $x $y $z $w			# ... but all statements are evaluated and variables saved.
```

See the file `fibonacci.sh` for a simple example of taking in user input for shell calculations.


## Expr

The command `expr` evaluates an expression of strings or decimal integers, optionally signed, and writes to standard output; any 
other operand is converted to an integer or string, depending on the operation being applied to it. Operators and operands must be 
separated by spaces and operators may need to be escaped. For example;

$ expr 1 + 2 + 3	# Addition. Note separation of operators and operands
$ expr 1+2+3		# Note no separation; treated as a whole.
$ expr 100 - 9		# Subtraction
$ expr 100 / 9		# Division. Note that the result in an integer
$ expr 100 * 9		# An error; multiplication operator must be escaped.
$ expr 100 \* 9		# Multiplication; metacharacter now a literal.
$ expr 20 % 3		# Modulus i.e., 20/3 = 6 residual 2.
$ expr 29 % 30		# Modulus, how much is left over if less than 1?
$ expr 2 \> 10		# Greater than, escape the metacharacter, returns false (0)
$ expr 2 \< 10		# Less than, escape the metacharacter, returns true (1)
$ expr 2 = 2		# Equals. Returns true (1), thankfully. 
$ expr 2 \<= 2		# Less than or equal to.
$ expr 2 \>= 2		# Greater than or equal to.
$ expr A = A		# String test for equality, returns true. Ayn Rand likes this.
$ expr a = A		# String test for equality, returns false. Operating system is case-sensitive.
$ expr a + b		# Addition of strings generates error.
$ expr a \< b		# Comparison of strings is based on alphabetical order.

Strings are not quotes although one may wish to do this to protect against special or metacharacters (e.g., spaces). Quoted numbers 
are treated like strings. Special operators for strings include length, index, and count of matching characters with regular 
expression meta-characters.

$ expr "1 + 2"					# This is a string
$ expr length "This is a long string"		# Length includes spaces.
$ expr index "This is a long string" "l"	# Index requires $string first, value second.
$ expr substr "This is a long string" 6 2	# Starting from character 6, provide next 2 characters.
$ expr "This is a long string" : "This is"	# Count matching character
$ expr "This is a long string" : "This is l"	# Characters must be contiguous
$ expr "This is a long string" : "^This"	# RegEx metacharacts apply; `^` is implicit.
$ expr "This is a long string" : "This$"	# RegEx metacharacters apply
$ expr match "This is a string" "This$"		# Alternative representation, RegEx metacharacters apply. 

substr STRING POS LENGTH 	substring of STRING, POS counted from 1

Previous examples showed comparisons in numerical expressions. However, these values are arguments and can equally apply with 
variables. The following illustrates this use, along with with AND/OR evaluation.

$ value1=20; value2=30			# Set some shell variables.
$ test1=$(expr $value1 \<= $value2)	# Set a variable that evaluates an expr command.
$ test2=$(expr $value1 \> $value2)	# Set a variable that evaluates an expr command.
$ action=$(expr $test1 \| $test2)	# Use of logical 'OR' (other option, logical AND with `&`)
$ echo $action				# Provide result.


Although `expr` may seem to be quite limited, operating on integers only, apart from these mathematical functions note that it can 
also be used with filenames with integer values, or sub-values, such as in HPC job arrays. In addition, with GNU parallel, some 
calculations can be arranged to make use of a multicore environment. e.g., `parallel echo {1} '$(expr {2} \* 2 )' ::: {1..2} ::: 
{1..2}`

