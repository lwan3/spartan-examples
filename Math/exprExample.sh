#!/bin/bash

# Example derived from Stack Overflow
# https://stackoverflow.com/questions/28522326/extracting-substring-in-linux-using-expr-and-regex

# Reads a line from the file views.txt
# 

while read line
	do
		out=$(expr "$line" : ".*<li>\(.*\) views")
	echo $out
done < views.txt

exit
