# Symbolic Computation with Maxima

## Derived from Lev Lafayette, Mathematical Applications and Programming: R, Octave, and Maxima, Victorian Partnership for Advanced 
## Computing, 2015. ISBN: 978-0-9943373-2-0

## Basics of Maxima

Maxima is a computer algebra system (CAS) derived from Macsyma. Macsyma's core design was established in 1968, and coding began in 
1969. In 1982, the project was split with a commercial arm which continued in development until 1999. Maxima is released under GPL. 
Maxima is written in Common Lisp and designed to run on all POSIX systems including Linux, Mac OSX, and also with ports for 
MS-Windows.

Once installed Maxima can be invoked on the command line which will put the user in a lisp-based intrepretative shell. Commands are 
terminated with a semicolon with notation to differentiate between user input and program output. Help is not invoked with the 
`help` command but rather with `describe(topic)`, `example(topic)` or `? topic`, for each function. The command `apropos` will 
search flags and functions with a particular string. A demonstration of functions can be invoked by the `demonstrate (topic)` 
command. To quit an interactive session use the `quit()` function.

```
$ sinteractive --ntasks=2 --partition=snowy --time=05:00:00
$ module load maxima/5.41.0
$ module load gnuplot/5.2.8
$ maxima
```

```
(%i1) help;
(%o1)      type `describe(topic);' or `example(topic);' or `? topic'
(%i2) describe (integrate);
..
(%i3) example(integrate);
..
(%i4) test(f):=block([u],u:integrate(f,x),ratsimp(f-diff(u,x)))
(%o4) test(f) := block([u], u : integrate(f, x), ratsimp(f - diff(u, x)))
(%i5) test(sin(x))
..
..
(%i21) quit();
```

There is a test suite which can be run. By default it does not show results, but this can be forced by a function parameter. This 
will, by the way, generate a lot of output. An alternative input terminator to the semicolon is the dollar sign ($), which 
suppresses the display. The following command is for a bug_report, which provides a URL for where to report bugs to, and the Maxima 
build information.

```
(%i1) run_testsuite(display_all = true);
..
..
..
```

A computation can be aborting without exiting Maxima with ^C - which is very handy if a computation is taking too long due to an 
input error. To repeat a user input command, place two single quotes quotes before the input line number. To refer to the output 
result, either use the o label or a percent symbol.

```
(%i1) run_testsuite(display_all = true);
..
..
^CEvaluation took:
(%i1) plot2d(x^2-x+3,[x,-10,10]);
(%o1)                  [/home/lev/maxout.gnuplot_pipes]
(%i2) ''%i1;
(%o2)                  [/home/lev/maxout.gnuplot_pipes]
```

User input can be established through the `read()` function. It simple reads in an expression from the console and returns the 
evaluated expression. The expression needs to be terminated with a semi-colon.

```
(%i5) expr: 42;
(%o5) expr: read;
(%i6) expr: read ("The current value is", expr. "Provide the new answer.")$
The current value is 42 Provide the new answer. 
36;
```

The option variable `ibase` is used to change the base of integers read by Maxima, and `obase` is the value for integers displayed. 
Both can have any value from 2 and 36 (in decimal base). If either is set to a value greater than 10, letters replace those values 
over 9. Note that the output includes the idenifier and input line numbers. Base-2 is, of course, a popular choice.

```
(%i8) obase : 2$
(%o1000)                              100100
(%i110) obase : 10$ 
(%o9)
```

The `playback()` function displays input, output, and intermediate expressions, without recomputing them, a very handy tool for 
reviewing previous work in a session. It only displays the expressions; other output (e.g., error messages) is not displayed. It is 
usual to bind the playback function with a range `playback ([<m>, <n>])` to displays input, output, and intermediate expressions 
with numbers from <m> through <n>. Without such parameters all expressions from a session are displayed. Another common option is 
`playback(slow)`, which pauses between expressions and waits for the user to press enter before moving to the next expression.

```
(%i9) playback(slow);
..
(%o9) done
(%o10) quit();
```

## Operators, Expressions, and Datatypes

Whilst Maxima is primarily designed for symbolic computation it can also do numeric computations with arbitrary precision and size 
determined by the computers hardware. The usual operations can be conducted with `+` (addition), `\` (division), `*` 
(multiplication), `^` (exponent), and `!` (factorials). Maxima uses the standard order of operations, with primacy given to 
equations in parantheses.

```
(%i1) 1+1+2*3;
(%o1)                                  8
(%i2) (1+1+2)*3;
(%o2)                                 12
(%i3) (1+1)*2^3;
(%o3)                                 16
```

Maxima also allows the user to refer to the latest result through the % character, or to any previous input or output by its 
respective prompted %i (input) or %o (output) value.

```
(%i4) %^5;
(%o4)                               1048576
(%i5) %o1+%o2+%o3;
(%o5)                                 74
```

In order to assign a value to a variable, Maxima uses the colon (:), not the equal sign. The equal sign is used for representing 
equations.  Functions are assigned through ‘:=’. Plots of functions, like any other plot, assigns the variable, the function, and 
the minimum and maximum range.

Maxima will tend to give symbolic results (i.e., results including fractions, square roots, unevaluated trigonometric, exponential, 
or logarithmic functions) rather than floating-point (or numerical) results. Use function float() to get floating-point solutions.

```
(%i6) ses:7$ ok:8$ nau:9$
(%i9) sqrt(ses^2+ok^2+nau^2);
(%o9)                              sqrt(194)
(%i10) float(sqrt(ses^2+ok^2+nau^2));
(%o10)                         13.92838827718412
(%i11) f(x) := sqrt(x);
(%o11)                          f(x) := sqrt(x)
(%i12) f(4);
(%o12)                                 2
(%i13) f(25);
(%o13)                                 5
(%i14) plot2d(sqrt(x),[x,0,1000]);
(%o14)                 [/home/lev/maxout.gnuplot_pipes]
(%i15) plot3d(x^2-y^2,[x,-4,4],[y,-4,4],[grid,16,16]);
```

The percentage (%) symbol represents the most recent result. The expand function multiplies out product and exponentiated sums, and 
expands sub-expressions.

```
(%i16) sin(20) + cos(20) + 1;float(%);
(%o16)                       sin(20) + cos(20) + 1
(%o17)                         2.32102731254102
(%i17) expr:(x+1)^2*(y+1)^3;
                                      2        3
(%o17)                         (x + 1)  (y + 1)
(%i18) expand(expr);
        2  3        3    3      2  2        2      2      2                    2
(%o18) x  y  + 2 x y  + y  + 3 x  y  + 6 x y  + 3 y  + 3 x  y + 6 x y + 3 y + x
                                                                      + 2 x + 1

```

The following are reserved words in Maxima and cannot be used as variable names;

```and at diff do else  elseif for from if in integrate limit next or product step sum then thru unless while```

Some common constants and functions in Maxima include the following, with the usual Hellenic letters used by name for some 
constants. A complex expression is specified by adding the real part of the expression to %i times the imaginary part.


| Value			| Maxima Representation	|
|:----------------------|:----------------------|
| Natural log		| %e			|
| Square root of -1	| %i			|
| Pi 			| %pi			|
| Phi, the Golden Mean	| %phi			|
| Euler's constant	| %gamma		|
| Positive real infinity| inf			|
| Negative real infinity| minf			|
| Complex infinity	| infinity		|
| Boolean values	| true, false		|
| Squart root		| sqrt			|
| tangent		| tan			|
| sine			| sin			|
| cosine		| cos			|
| exponential		| exp			|
| Absolute value	| abs			|
| Indefinite result	| ind			|
| Undefined result	| und			|

Maxima only offers the natural logarithm function log. log10 is not available by default but can be defined as a function:

```
(%i19) log10(x):= log(x)/log(10);
                                          log(x)
(%o19)                        log10(x) := -------
                                          log(10)

(%i20) log10(10);
(%o20)                                 1
(%i21) log10(1000);
                                   log(1000)
(%o21)                             ---------
                                    log(10)
```

As mentioned, the function float is to get floating-point solutions. This conversts all integraters, rational numbers, and bigfloats 
to floating point. The function bfloat() will convert numbers in an expression to bigfloat numbers, with the number of significant 
digits specified by the global variable `fpprec`. A conversion from floats to bigfloats can be carried out with the `float2bf()` 
function.

The `rationlise()` function will convert all double floats and big floats in an expresion to their rational equivalents. These 
values are exact, and as a result can lead to some surprising results. Note how in the following, the value 0.1 does not equal 1/10, 
as the latter real has a repeating (not terminating) binary representation to the precision of the system.

```
(%i22) rationalize (sin (0.1*x + 5.6));
                       3602879701896397 x   3152519739159347
(%o22)             sin(------------------ + ----------------)
                       36028797018963968    562949953421312
```


Maxima draws a distinction between its numerical, symbolic, and string data types. Strings are represented as double-quoted 
character sequences, and can include any characters (including tabs, newlines, carriage returns etc), with the backslash as an 
escape metacharacter to display values such as the literal double-quote. Note that there is a no type 'character' in Maxima - this 
would be a string of one-character length.

The concat() function concatanes arguements and evaluates to atomic components. The return value is a symbol if the first argument 
is a symbol and a string otherwise. The single quote prevents evaluation.

A symbol constructed by concat may be assigned a value and appear in expressions. The :: (double colon) assignment operator 
evaluates its left-hand side. Whilst operations may occur on values prior to evaluation, their output is in fact a string, even if 
they look like a number.

The function `string()` converts an expression to a string.

```
(%i23) stringed: concat(ses,ok,nau);
(%o23)                                789
(%i15) concat('ses,ok,nau);                     
(%o15)                               ses89
(%i16) stringed2: concat(ses*2,ok*2,nau*2);
(%o16)                              141618
(%i17) stringedplus: concat(ses,ok,nau) + 11;
(%o17)                             789 + 11
```

Apart from data types, data in Maxima can also be expressed in structures. A typical data structure is a list, expressed within 
brackets and from which elements can be associated. For those who know the programming language Lisp, which Maxima is written, the 
importance of the list in that language is equivalent in Maxima.

The `append()` function can add elements to a list, whereas a `delete()` function will remove elements. The function `makelist()` 
creates an empty list by itself; the function parameter creates elements according to the expression, in a general expression of 
`makelist (expr, i, i_0, i_max, step)`. In the following example the random function is called to simulate a ten throws of a 
six-sided dice.

```
(%i1) listo: [ses, sep, ok];
(%o1)                           [ses, sep, ok]
(%i2) listo[3];
(%o2)                                 ok
(%i13) listo : append ([nulo, unu, du, tri, kvar, kvin],[ses, sep, ok, nau]);
(%o13)        [nulo, unu, du, tri, kvar, kvin, ses, sep, ok, nau]
(%i14) listo;
(%o14)        [nulo, unu, du, tri, kvar, kvin, ses, sep, ok, nau]
(%i20) listo : delete(nulo,[nulo, unu, du, tri, kvar, kvin, ses, sep, ok, nau]);
(%o20)           [unu, du, tri, kvar, kvin, ses, sep, ok, nau]
(%i24) dice : makelist(random(5)+1,10);
(%o24)                  [5, 5, 4, 5, 2, 2, 2, 4, 1, 2]
```

Lists can be treated as explicit vectors. As a result, operations can be performed on lists in association with other lists.

```
(%i28) vectorsym: [a, b, c, d];                                                
(%o28)                           [a, b, c, d]
(%i29) vectornum: [1, 2, 3, 4];
(%o29)                           [1, 2, 3, 4]
(%i30) vectorsym + vectornum;
(%o30)                   [a + 1, b + 2, c + 3, d + 4]
(%i31) vectorsym - vectornum;
(%o31)                   [a - 1, b - 2, c - 3, d - 4]
(%i32) vectorsym * vectornum;
(%o32)                        [a, 2 b, 3 c, 4 d]
(%i33) vectorsym / vectornum; 
                                     b  c  d
(%o33)                           [a, -, -, -]
                                     2  3  4
```

An array is a multi-dimensional list. The command `array (<name>, <type>, <dim_1>, ..., <dim_n>)` creates an array, with elements of 
a specified type, such as 'fixnum' for integers 'flonum' for floating-point numbers. The number of dimension is less than or equal 
to 5. If the array is assigned to a subscripted variable before declaring the corresponding array, an undeclared array is created, 
which is more typical. Undeclared arrays grow dynamically by hashing as more elements are assigned values.

The function `arrayinfo()` will return information about the array specified in a paramter. For declared arrays, arrayinfo returns a 
list including the number of dimensions, and the size of each dimension.  For undeclared arrays, arrayinfo returns a list including 
the number of subscripts, and the subscripts of every element which has a value. For array functions or subscripted functions, 
arrayinfo returns a list including the number of subscripts, and any subscript values for which there are stored function values, or 
lambda expressions, respectively. In all cases the elements of the array, are returned by `listarray()`.

To fill an array the `fillarray()` function is used, with two parameters. The first is the array to be filled, the second is a list 
or an existing array. If a specific type was declared for an array when it was created, it can only be filled with elements of that 
same type. If the dimensions of the parameters are different, the array is filled in row-major order. If there are not enough 
elements in the second parameter the last element is used to fill out the rest of array. If there are too many, the remaining ones 
are ignored.  Multple-dimension arrays are filled in row-major order.

```
(%i34) array (numeroj, fixnum, 10);
(%o34)                          numeroj
(%i35) listarray (numeroj);
(%o35)                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
(%i36) fillarray (,dice);
(%o36)                          numeroj
(%i37) listarray (numeroj);
(%o37)              [5, 5, 4, 5, 2, 2, 2, 4, 1, 2]
(%i38) array2d : make_array (fixnum, 2, 5);
(%o38)            {Lisp Array: #2A((0 0 0 0 0) (0 0 0 0 0))}
(%i39) fillarray (array2d, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
(%o39)            {Lisp Array: #2A((1 2 3 4 5) (6 7 8 9 10))}
```

A general data aggregate structure is an expression whose arguments (which can be datatypes, variables, expressions etc) are 
identified b the field name and the expression by the operator, the structure name.  Structures are defined by the `defstruct()` 
function. In the format `defstruct (S(a_1, ... , a_n))`, the list of named fields a_1,..., a_n is associated with a symbol S. The 
`new()` function creates a new instance of the structure with the structure name as the paramter.

The `@` is the operator used to access fields. The expression `S@a` refers to the value of field a of the structure instance S; the 
expression `kill(S@a)` removes the value of field a in x. The function `structures()` displays user-defined structures. The function 
`kill()` removes the structure.

```
(%i40) defstruct (vortoj (unu, du, tri, kvar, kvin));
(%o41)                [vortoj(unu, du, tri, kvar, kvin)]
(%i42) numeroj: new (vortoj (1, 2, 3, 4, 5));
(%o42)       vortoj(unu = 1, du = 2, tri = 3, kvar = 4, kvin = 5)
(%i43) numeroj@kvin;
(%o44)                                 5
(%i45) kill (numeroj@kvin);
(%o46)                               done
(%i47) numeroj;     
(%o47)         vortoj(unu = 1, du = 2, tri = 3, kvar = 4, kvin)
(%i48) structures();
(%o48) [ef_data(exponent, reduction, primitive, cardinality, order, 
factors_of_order), vortoj(unu, du, tri, kvar, kvin)]
(%i49) kill(vortoj);
(%o49)                               done
(%i50) quit();
```

## 3.5 Plots and Files
	
A highly pleasing feature of Maxima is its strong integration with gnuplot and thus the inclusion of X11 libraries. The `plot2d()` 
function is perhaps the most basic and popular plotting function. It can displays one or several plots in two dimensions, ad can 
accept expressions or function name to define the plots, as long they depend on one variable and have a mandatory x_range. Plots can 
also be defined in discrete or parametric forms. The discrete form is used to plot a set of points with given coordinates, starting 
with the keyword discrete, followed by one or two lists of values, reflecting x and y coordinates respectively. A parametric plot is 
defined by a list starting with the keyword parametric, followed by two expressions or function names and a range for the parameter. 
If there are several plots to be plotted, a legend will be written to identity each of the expressions.

For example, a plot of 10 random numbers between 0 and 99, followed by a simple sine function, and a simple parabola.

```
(%i1) plot2d ([discrete, makelist ( random(99), 10)])$
(%i1) plot2d (sin(x), [x, -%pi, %pi])$
(%i1) plot2d ( x^2 - 1, [x, -4, 4])$
```

With Maxima, it is not just two-dimensional graphs that can be plotted. The function `plot3d()` can display a plot of one or more 
surfaces as functions of two variables or as a parametic. The general expression is `plot3d ([expr_1, ..., expr_n], x_range, 
y_range, ..., options, ...)`.

The first example simply plots a function of two variables. The second uses the z option to limit a function that goes to infinity. 
This particular example removes shading. The third example is the expressions to generate a e Kleinsche Fläche (aka Klein bottle).

With the gnuplot output the mouse can be used to change the perspective, although this may incur some latency effects depending on 
the complexity of the plot.

```
(%i1) plot3d (x^2 - y^2, [x, -4, 4], [y, -6, 6], [grid, 100, 100], [mesh_lines_color,false])$
(%i1) plot3d ( log ( x^2*y^2 ), [x, -2, 2], [y, -2, 2], [z, -8, 4], [palette, false], [color, red])$
(%i1) expr_1: 5*cos(x)*(cos(x/2)*cos(y)+sin(x/2)*sin(2*y)+3)-10$
(%i2) expr_2: -5*sin(x)*(cos(x/2)*cos(y)+sin(x/2)*sin(2*y)+3)$
(%i3) expr_3: 5*(-sin(x/2)*cos(y)+cos(x/2)*sin(2*y))$
(%i4) plot3d ([expr_1, expr_2, expr_3], [x, -%pi, %pi], [y, -%pi, %pi], [grid, 50, 50])$
```

Ever attractive ("You can create art and beauty on a computer", Steven Levy, Hackers: Heroes of the Computer Revolution, 1984), the 
`mandelbrot()` function creates a graphic represenation of the Mandlebrot set. The function requires the additional package 
`dynamics()`, however it is automatically loaded with this one. If called without arguments, it will use a default value of 9 
iterations per point, a grid with dimensions set by the grid plot option (default 30 by 30) and a region that extends from -2 to 2 
in both axes. The deafult is pretty rough, so it's worthwhile to provide a greater number of iterations, a larger grid and 
specifiying the range.

Of a similar style, the `julia()` function creates a presentation for the complex number (x + i y), with the the x and y parameters 
mandatory. Like `mandelbrot()`', this function also requires the additional package `dynamics()` but is also automatically loaded. 
Optional paramters include the number of iterations, the x and y range, and, in this example, along with grid and colour options 
etc.

```
(%i10) mandelbrot();
(%o10)                    [/home/lev/maxout.gnuplot]
(%i1) mandelbrot ([iterations, 30], [x, -2, 1], [y, -1.2, 1.2], [grid,400,400])$
(%i1) julia (-0.55, 0.6, [iterations, 36], [x, -0.3, 0.2], [y, 0.3, 0.9], [grid, 400, 400], [color_bar_tics, 0, 6, 36])$
```

Moving out of the domain of imaginary numbers, the `implicit_plot()` function displays a plot of a function on the real plane, defined implicitly by the expression with a domain in the plane is defined by the x and y range parameters. It explicitly requires the `load(implicit_plot);` command. The `contour_plot()` function takes an expression, the x range and the y range as parameters, with additional optional generic plot paramters for colour etc - the example here is "legend false". 

```
(%i6) load(implicit_plot);
(%o6) 
  /usr/local/maxima/5.36.1/share/maxima/5.36.1/share/contrib/implicit_plot.lisp
(%i5) implicit_plot (x^2 = y^3 - 3*y + 1, [x, -4, 4], [y, -4, 4])$
(%i3) contour_plot (x^2 + y^2, [x, -4, 4], [y, -4, 4], [legend,false],)$
```

Examples of plot options have been provided in the preceeding examples. These are, as the name implies, optional, and with default 
values. For example the `colour [color0, color1, ..., colorn];` can be expressed in RGB components, initialised by a hash and 
followed two hexidecimal values per component, or the values red, green, blue, magenta, cyan, yellow, orange, violet, brown, gray, 
black, or white. If the name of a given colour is, black will be used instead. Anothere xample is the `grid [grid, integer, integer` 
option which sets the number of grid points in the x- and y- axes for three-dimensional plotting, or the `grid2d[grid, value]` for 
lines on the xy plane. The default value is 30,30 for the former and false for the latter.

Along with plotting, a common - and indeed essential - user operation in Maxima is file manipulation. The functions `appendfile()` 
and `writefile()` both append a consol transcript to a filename, which is useful for saving session results. This can be closed in a 
session with `closefile()`. Note that the command `save()` will creates a Lisp file, not a Maxima file.

To run a Maxima script of expressions, the `batch(filename)` command can be used, loading the filename (and path) as specified 
(e.g., `batch(~/maxima/test)`. An alternative is `batchload(filename)` which simply evaluates the script, without displaying input 
or output of the expressions within, and the `load(filename)` function which evaluates expressions in a filename, thus brininging 
variables, functions, and other objects into a Maxima session. If file name is passed to functions and the file name does not 
include a path, Maxima stores the file in the current working directory.

Consider the following file, `~/maxima/solver.mac`, and the differences between batch mode, and load.

```
f(x):=x^4+x-2;
roots:solve(f(x) = 0,x);
print("The roots of x^4 + x -1 are ",float(roots));
stringout("solver.mac",input);
```

```
(%i4) batch("~/maxima/solver.max");
read and interpret file: /data/user1/lev/maxima/solver.max
..
..
(%o8)                        ~/maxima/solver.max
(%i9) load("~/maxima/solver");
The roots of x^4 + x -1 are  [x = (- 1.204387239234341
...
(%o9)                        ~/maxima/solver.mac
(%i10)
```

## Polynominals

Polynomials are stored in Maxima in two ways; either as General Form or as Canonical Rational Expressions (CRE). The latter 
represents is particularly suitable for expanded polynominials and rational functions. As always, this is only a small selection of 
the specialist functions available in Maxima for polynominals, but it does include the most common cases.

As an example consider the `bothcoef()` function. This returns a list whose first member is the coefficient of `x` in `expr`, and 
whose second member is the remaining part of expr. That is, `[A, B]` where `expr = A*x + B`

```
(%i8) islinear (expr, x) := block ([c],
	c: bothcoef (rat (expr, x), x),
	is (freeof (x, c) and c[1] # 0))$

(%i9)  islinear ((r^2 - (x - r)^2)/x, x);
(%o9)                                true
```

A relatively simple but very useful function is `denom()` which takes an expression as an argument and returns the denominator. The 
following example is interesting.

```
(%i11) x1:((x+2)/(x-4))-((x+1)/(x+4));
                                 x + 2   x + 1
(%o11)                           ----- - -----
                                 x - 4   x + 4
(%i12) denom(x1);
(%o12)                                 1
```

The `coeff()` function returns the coefficient of `x^n` in `expr`, where `expr` is a polynominal or a monomial (one-term 
polynominal) in `x`. Other than the `ratcoef()` function `coeff()` is a strictly syntactical operation and will only find literal 
instances of `x^n` in the internal representation of expr. An elaboration, `coeff(expr, x^n)` is equivalent to `coeff(expr, x, n)`. 
If omitted, n is assumed to be 1 whereas `x` may be a simple variable or a subscripted variable, or a subexpression of `expr` which 
comprises an operator and all of its arguments. The function `coeff(expr, x^n)` is equivalent to `coeff(expr, x, n)`.  The `coeff()` 
function operates over lists, matrices, and equations.

```
(%i4) coeff (x^3 + 2*x^2*y + 3*x*y^2 + 4*y^3, x);
                                        2
(%o4)                                3 y
(%i7) (a[4]*x^3 - a[3]*x^2 - a[2]*x^2 + a[1]*x, x, 4);
(%o7)                                  4

(%i3) coeff (sin(1+x)*sin(x) + sin(1+x)^2*sin(x)^2, sin(1+x)^2);
                                       2
(%o3)                               sin (x)
```

The `ratcoef ()` function returns the coefficient of the expression `x^n` in the expression `expr`, again if omitted, `n` is assumed 
to be 1. The return value is free of the variables in `x`. If no coefficient of this type exists, 0 is returned. The `ratcoef()` 
function expands and rationally simplifies its first argument and so it may produce answers different from those of the `coeff()` 
which is purely syntactic. The function `ratcoef (expr, x, 0)`, viewing expr as a sum, returns a sum of those terms which do not 
contain x. If x occurs to any negative powers, ratcoef should not be used.

```
(%i6) coef ((x + 1)/y + x, x);
                                   x + 1
(%o6)                         coef(----- + x, x)
                                     y
(%i8) ratcoef ((x + 1)/y + x, x);
                                     y + 1
(%o8)                                -----
                                       y
```

Elaborating further, the function `resultant()` computes the resultant, a polynomial expression of the coefficients of the two 
polynomials p1 and p2, eliminating the variable x, i.e., `resultant(p1, p2, x)`. The resultant is a determinant of the coefficients 
of x in p1 and p2, which equals zero if and only if p1 and p2 have a non-constant factor in common. If p1 or p2 can be factored, it 
is recommended to use the factor before calling resultant.

```
(%i9) resultant(2*x^2+3*x+1, 2*x^2+x+1, x);
(%o9)                                  8
(%i10) resultant(x+1, x+1, x);
(%o10)                                 0
```

The option variable `resultant` controls which algorithm will be used to compute the resultant with the function resultant. The 
possible arguments are `subres` which uses the subresultant polynomial remainder sequence (PRS) algorithm, `mod` which uses the 
modular resultant algorithm, and `red` for reduced polynomial remainder sequence (PRS) algorithm. In most cases the default value 
subres should be best, however in for large degree univariate or bivariate problems `mod` may be better. The `determinant` of this 
matrix is the desired resultant.

The function `bezout()` is an alternative to the `resultant()` command, and returns a matrix. 

```
(%i11) bezout (2*x^2+3*x+1, 2*x^2+x+1, x);
                                  [  0   2 ]
(%o11)                            [        ]
                                  [ - 4  0 ]
(%i12) determinant(%);
(%o12)                                 8
(%i13) bezout (x+1, x+1, x);
(%o13)                               [ 0 ]
(%i14) determinant(%);
(%o14)                                 0
```

The `factor()` function in Maxima requires some attention. By itself it factions an expression, which contains any number of 
variables or functions, into factors irreducible over the integers, i.e., `factor(exp)`. An alternative, `factor(expr, p)` factors 
the argument expr over the field of rationals with the polynominal `p` as an adjoined element.

In addition to this however, there are a number of variables and other functions which the factor function uses to determine its 
behaviour. For example, factor uses the `ifactors()` function when factoring integers. The following also illustrates a simple 
check.

```
(%i16) factor (2^32-1);
(%o16)                         3 5 17 257 65537
(%i25) 2^32-1;
(%o25)                            4294967295
(%i24) 3*5*17*257*65537;
(%o24)                            4294967295

```

An optional variable, `factorflag` is set to false by default, This suppresses the factoring of integer factors of rational 
expressions. In contrast, the optional variable `berlefact` is set to true by default, which uses the the Berlekamp algorithm for 
factoring algorithms, as the name indicates. If it is set to false, then the Kronecker algorithm is used instead. The optional 
variable `savefactors` is also set to true by default. This causes the factors of an expression that is a product of factors to be 
saved by some functions in order to speed up later factorisations of expressions containing some of the same factors.

The function `factorout()`, consists of an expression and two or values as parameters. As the name indicates, the function 
rearranges the sum of the expression into the sum of the terms (x_1, x_2, ...)*g, where g is the product of expressions not 
containing any x_i and f is factored. The optional variable keepfloat is ignored by factorout.

```
(%i25) expand (a*(x+1)*(x-1)*(u+1)^2);
                2  2          2      2      2
(%o25)     a u  x  + 2 a u x  + a x  - a u  - 2 a u - a
(%i26) factorout(%,x);
             2
(%o26) a u  (x - 1) (x + 1) + 2 a u (x - 1) (x + 1)
                                                  + a (x - 1) (x + 1)
```

The related function `factorsum()` takes an expression as a parameter and groups terms in factors of that expression which are sums 
in groups of terms, as long as their sum is factorable. For example `factorsum()` can recover the result of `expand ((x + y)^2 + (z 
+ w)^2)` but it can't recover expand `((x + 1)^2 + (x + y)^2)` because the terms have variables in common.

```
(%i27) expand ((x + 1)*((u + v)^2 + a*(w + z)^2));
               2      2                            2      2
(%o28) a x z  + a z  + 2 a w x z + 2 a w z + a w  x + v  x

                                         2        2    2            2
                            + 2 u v x + u  x + a w  + v  + 2 u v + u
(%i29) factorsum (%);
                                       2          2
(%o29)            (x + 1) (a (z + w)  + (v + u) )
```

The fuction `gfactor()` takes factors a polynomial expression parameter over Gaussian integers (that is, the integers with the imaginary unit %i adjoined). This is like factor (expr, a^2+1) where a is %i. The `gcfactor()` function factors a Gaussian integer as a parameter over the Gaussian integers, i.e., numbers of the form a + b %i where a and b are rational integers (i.e., ordinary integers). Factors are normalized by making a and b non-negative. The function `gfactorsum()` is essentially the same as the `factorsum` function but applies `gfactor()` of `factor()`.

```
(%i30) gfactor (x^4 - 1);
(%o30)           (x - 1) (x + 1) (x - %i) (x + %i)
```

Finally, the `dontfactor` identifier is set to a list of variables in a block in which factoring is not to occur; by default is an empty set. 

```
(%i25) block ([dontfactor: [x]], factor (%/36/(1 + 2*y + y^2)));
                      2
                     (x  + 2 x + 1) (y - 1)
(%o25)                ----------------------
                           36 (y + 1)
```

## Programming Maxima

Maxima is written in Lisp and as a result it easy to access Lisp functions and variables from Maxima and vice versa. The assumption here is that those interested in programming Maxima are already familiar with Lisp - if not they'll need to be! This section deals only with the integration and special conventions that Maxima uses within Lisp. For example, Lisp and Maxima symbols are distinguished by naming conventions. A Lisp symbol which begins with a dollar sign `$` corresponds to a Maxima symbol without the dollar sign, whereas a Maxima symbol which begins with a question mark `?` corresponds to a Lisp symbol without the question mark. The hyphen `-`, asterisk `*`, or other special characters in Lisp symbols must be escaped by backslash `\` where they appear in Maxima code. 

Lisp code may be executed from within a Maxima session. A single line of Lisp (containing one or more forms) may be executed by the special command :lisp. For example, to calls the Lisp function `foo` with Maxima variables `x` and `y` as arguments. The `:lisp` construct can appear at the interactive prompt or in a file processed by batch or demo, but not in a file processed by `load`, `batchload`, `translate_file`, or `compile_file`.

`(%i1) :lisp (foo $x $y)`

The function `to_lisp` opens an interactive Lisp session. Entering `(to-maxima)` closes the Lisp session and returns to Maxima. If Lisp functions and variables are to be visible in Maxima as functions and variables they must use with ordinary names (i.e., no special punctuation) and must have Lisp names beginning with the dollar sign $. In addition, Maxima is case-sensitive, distinguishing between lowercase and uppercase letters in identifiers. Therefore are some rules governing the translation of names between Lisp and Maxima.

* A Lisp identifier not enclosed in vertical bars corresponds to a Maxima identifier in lowercase. Whether the Lisp identifier is 
uppercase, lowercase, or mixed case, is ignored. E.g., Lisp `$foo`, `$FOO`, and `$Foo` all correspond to Maxima `foo`. This is 
because `$foo`, `$FOO` and `$Foo` are converted by the Lisp reader by default to the Lisp symbol `$FOO`. 

* A Lisp identifier which is all uppercase or all lowercase and enclosed in vertical bars corresponds to a Maxima identifier with 
case reversed. That is, uppercase is changed to lowercase and lowercase to uppercase. E.g., Lisp `|$FOO|` and `|$foo|` correspond to 
Maxima `foo` and `FOO`.

* A Lisp identifier which is mixed uppercase and lowercase and enclosed in vertical bars corresponds to a Maxima identifier with the 
same case. E.g., Lisp `|$Foo|` corresponds to Maxima `Foo`.

The `#$ Lisp` macro allows the use of Maxima expressions in Lisp code and `#$expr$` expands to a Lisp expression equivalent to the Maxima expression expr. The Lisp function `displa` prints an expression in Maxima format.

```
(%i1) :lisp #$[x, y, z]$ 
((MLIST SIMP) $X $Y $Z)
(%i1) :lisp (displa '((MLIST SIMP) $X $Y $Z))
[x, y, z]
NIL
```

Functions defined in Maxima are not ordinary Lisp functions. The Lisp function `mfuncall` calls a Maxima function. For example:

```
(%i1) foo(x,y) := x*y$
(%i2) :lisp (mfuncall '$foo 'a 'b)
((MTIMES SIMP) A B)
```

Maxima provides a `do` loop for iteration. There are three variants of `do` that differ only in their terminating conditions, which are similar to other programming languages. They are:

* for variable: initial_value, step increment, to limit do body
* for variable: initial_value, step increment, while condition do body
* for variable: initial_value, step increment, unless condition do body 

```
(%i1) for a:-3 thru 26 step 7 do display(a)$
                             a = - 3

                              a = 4

                             a = 11

                             a = 18

                             a = 25

(%i1) s: 0$
(%i2) for i: 1 while i <= 10 do s: s+i;
(%o2)                         done
(%i3) s;
(%o3)                          55
```

A `return()` may be used to explicity break from a current block bringing its argument, comparable to other programming languages. However in Maxima the `return()` only returns from the current block, not from the entire function it was called in. In this aspect it more closely resembles the break statement from C.

```
    (%i1) for i:1 thru 10 do o:i;
    (%o1)                         done
    (%i2) for i:1 thru 10 do if i=3 then return(i);
    (%o2)                           3
    (%i3) for i:1 thru 10 do
        (
            block([i],
                i:3,
                return(i)
            ),
            return(8)
        );
    (%o3)                           8
    (%i4) block([i],
        i:4,
        block([o],
            o:5,
            return(o)
        ),
        return(i),
        return(10)
     );
    (%o4)                           4
```

Conditional evaluations are carried out with the special operator `if`, which takes the general form: `if cond_1 then expr_1 else 
expr_0` evaluates to `expr_1 if cond_1` evaluates to true, otherwise the expression evaluates to `expr_0`. The command `if cond_1 
then expr_1 elseif cond_2 then expr_2 elseif ... else expr_0` evaluates to `expr_k` if `cond_k` is true and all preceding conditions 
are false. If none of the conditions are true, the expression evaluates to `expr_0`. A trailing `else` false is assumed if else is 
missing. The alternatives `expr_0, ..., expr_n` may be any Maxima expressions, including nested `if` expressions. The conditions 
`cond_1,..., cond_n` are expressions which potentially or actually evaluate to true or false. When a condition does not actually 
evaluate to true or false, the behavior of if is governed by the global flag `prederror`. Common symbolic expressions are used for 
conditional statements:

|-------------------------|---------------|---------------------|
|: Operation		  |: Symbol	  |: Type               |
|-------------------------|---------------|---------------------|
| less than 		  | <		  | relational infix    |
| less than or equal to   | <=  	  | relational infix    |
| equality (syntactic)    | =             | relational infix    |
| negation of =           | #             | relational infix    |
| equality (value)        | equal         | relational function |
| negation of equal       | notequal      | relational function |
| greater than or equal to| >=	          | relational infix    |
| greater than            | >             | relational infix    |
| and 	                  | and           | logical infix       |
| or                      | or            | logical infix       |
| not                     | not           | logical prefix      |
|-------------------------|---------------|---------------------|
