#!/bin/bash

# To give your job a name, replace "MyJob" with an appropriate name
#SBATCH --job-name=Metagenome-test.slurm

# Run multicore
#SBATCH --ntasks-per-node=4

# Set your minimum acceptable walltime=days-hours:minutes:seconds
#SBATCH -t 0:45:00

# Specify your email address to be notified of progress.
# SBATCH --mail-user=youremailaddress@unimelb.edu.au
# SBATCH --mail-type=ALL

# Load the environment variables
module purge
module load foss/2019b
module load fastqc/0.11.9-java-11.0.2
module load sickle/1.33
module load megahit/1.1.4-python-3.7.4
module load bowtie2/2.3.5.1
module load samtools/1.9
module load bcftools/1.9
module load metabat/2.12.1-python-2.7.16
module load checkm/1.1.2-python-3.7.4

# Use FastQC to check the quality of our data

cd results/
ln -s ../data/tara_reads_* .
fastqc tara_reads_*.fastq.gz

# Trim the reads using sickle

sickle pe -f tara_reads_R1.fastq.gz -r tara_reads_R2.fastq.gz -t sanger \
    -o tara_trimmed_R1.fastq -p tara_trimmed_R2.fastq -s /dev/null

# Assemble with MEGAHIT
megahit -1 tara_trimmed_R1.fastq -2 tara_trimmed_R2.fastq -o tara_assembly

# Map the reads back against the assembly to get coverage information
ln -s tara_assembly/final.contigs.fa .
bowtie2-build final.contigs.fa final.contigs
bowtie2 -x final.contigs -1 tara_reads_R1.fastq.gz -2 tara_reads_R2.fastq.gz | \
    samtools view -bS -o tara_to_sort.bam
samtools sort tara_to_sort.bam -o tara.bam
samtools index tara.bam

# Get the bins from metabat
runMetaBat.sh -m 1500 final.contigs.fa tara.bam
mv final.contigs.fa.metabat-bins1500 metabat

# Check the quality of the bins

checkm lineage_wf -x fa metabat checkm/

## Below has been deprecated ##
# Plot the completeness
# checkm bin_qa_plot -x fa checkm metabat plots
# Take a look at plots/bin_qa_plot.png
