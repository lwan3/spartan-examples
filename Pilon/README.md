Content derived from the Swedish University of Agricultural Sciences

https://www.hadriengourle.com/tutorials/

Pilon is a software tool which can be used to automatically improve draft assemblies. It attempts to make improvements to the input 
genome, including:

Single base differences
Small Indels
Larger Indels or block substitution events
Gap filling
Identification of local misassemblies, including optional opening of new gaps

Before running Pilon itself, we have to align our reads against the assembly

bowtie2-build m_genitalium.fasta m_genitalium
bowtie2 -x m_genitalium -1 ERR486840_1.fastq.gz -2 ERR486840_2.fastq.gz | \
    samtools view -bS -o m_genitalium.bam
samtools sort m_genitalium.bam -o m_genitalium.sorted.bam
samtools index m_genitalium.sorted.bam

then we run Pilon


pilon --genome m_genitalium.fasta --frags m_genitalium.sorted.bam --output m_genitalium_improved

which will correct eventual mismatches in our assembly and write the new improved assembly to m_genitalium_improved.fasta


