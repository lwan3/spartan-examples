#!/bin/bash

#SBATCH --ntasks=XXXmpinodesXXX
#SBATCH --partition=XXXqueueXXX
#SBATCH --qos=gpgpuhpcadmin
#SBATCH --gres=gpu:4
#SBATCH --cpus-per-task=XXXthreadsXXX
#SBATCH --time=1:00:00
#SBATCH --mem-per-cpu=12g
#SBATCH --gres=gpu:2

srun XXXcommandXXX
