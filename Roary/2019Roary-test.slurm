#!/bin/bash

# To give your job a name, replace "MyJob" with an appropriate name
#SBATCH --job-name=Roary-test.slurm

# Allocate four CPUs
#SBATCH --ntasks-per-node=4

# Set your minimum acceptable walltime=days-hours:minutes:seconds
# Yes, this is a fair-sized job
#SBATCH -t 10:00:00

# Specify your email address to be notified of progress.
# SBATCH --mail-user=youremailaddress@unimelb.edu.au
# SBATCH --mail-type=ALL

# Load the environment variables
module purge
module load foss/2019b
module load fastqc/0.11.9-java-11.0.2
module load megahit/1.1.4-python-3.7.4
module load prokka/1.14.5
module load enabrowsertool/1.5.4-python-3.7.4
module load roary/3.12.0-perl-5.30.0
module load web_proxy 

# Get some random strains from the dataset
cut -d',' -f 1 journal.pcbi.1006258.s010.csv | tail -n +2 | shuf | head -10 > strains.txt

# Fastqc

cat strains.txt | parallel enaGroupGet -f fastq {}

mkdir reads
mv ERS*/*/*.fastq.gz reads/
# rm -r ERS*

# Assemble with Megahit

mkdir assemblies
for r1 in reads/*_1.fastq.gz
do
    prefix=$(basename $r1 _1.fastq.gz)
    r2=reads/${prefix}_2.fastq.gz
    megahit -1 $r1 -2 $r2 -o ${prefix} --out-prefix ${prefix}
    mv ${prefix}/${prefix}.contigs.fa assemblies/
    rm -r ${prefix}
done

# Prokka to annotate

mkdir annotation
for assembly in assemblies/*.fa
do
    prefix=$(basename $assembly .contigs.fa)
    prokka --usegenus --genus Escherichia --species coli --strain ${prefix} \
        --outdir ${prefix} --prefix ${prefix} ${assembly}
    mv ${prefix}/${prefix}.gff annotation/
#    rm -r ${prefix}
done

# Run roary for pan-genpmic analysis

roary -f roary -e -n -v annotation/*.gff

# Prepare for visualising plots

cd roary
FastTree -nt -gtr core_gene_alignment.aln > my_tree.newick
