Salmon is a fast program to produce a highly-accurate, transcript-level quantification estimates from RNA-seq data.

https://www.hadriengourle.com/tutorials/

This example derived from the Swedish University of Agricultural Sciences

For this tutorial we will use the test data from this paper:

http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004393

The test data consists of two commercially available RNA samples: Universal Human Reference (UHR) and Human Brain Reference (HBR). 
The UHR is total RNA isolated from a diverse set of 10 cancer cell lines.

Salmon goes through each sample and invokes salmon using fairly basic options:

The -i argument tells salmon where to find the index 

--libType A tells salmon that it should automatically determine the library type of the sequencing reads (e.g. stranded vs. 
unstranded etc.)

The -1 and -2 arguments tell salmon where to find the left and right reads for this sample (notice, salmon will accept gzipped FASTQ 
files directly). 

the -o argument specifies the directory where salmon’s quantification results sould be written.
