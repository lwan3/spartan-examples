Archtitecture Considerations
============================

Spartan login nodes have connection to the public Internet.

Spartan compute nodes do not have connection to the public Internet.

When trying to connect to the public Internet via a compute node, whether as part of a Slurm script or an interactive job, the task will fail.

e.g.,
[lev@spartan-rc035 ~]$ wget https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Linux_kernel_map.png/800px-Linux_kernel_map.png
--2019-11-25 10:56:06--  https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Linux_kernel_map.png/800px-Linux_kernel_map.png
Resolving upload.wikimedia.org (upload.wikimedia.org)... 103.102.166.240, 2001:df2:e500:ed1a::2:b
Connecting to upload.wikimedia.org (upload.wikimedia.org)|103.102.166.240|:443... failed: No route to host.
Connecting to upload.wikimedia.org (upload.wikimedia.org)|2001:df2:e500:ed1a::2:b|:443... failed: Network is unreachable.

In contrast, if the module web_proxy is loaded a proxy environment path will be established which allows connection to the public Internet.

Try:

sbatch fail.slurm
sbatch succeed.slurm

Run an interactive job and compare the the environment.

[lev@spartan-login1 ~]$ sinteractive
srun: job 12960294 queued and waiting for resources
srun: job 12960294 has been allocated resources
[lev@spartan-rc035 ~]$ module load web_proxy
[lev@spartan-rc035 ~]$ env | grep 'proxy=http'
[lev@spartan-rc035 ~]$ env | grep 'proxy=http'
http_proxy=http://wwwproxy.unimelb.edu.au:8000
ftp_proxy=http://wwwproxy.unimelb.edu.au:8000
https_proxy=http://wwwproxy.unimelb.edu.au:8000

